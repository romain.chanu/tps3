<?php
session_start();

if (!function_exists("xml_get_current_line_number")) {
  die("php-xml missing");
}


require __DIR__.'/app/config.php';
require __DIR__.'/app/common.php';
require __DIR__.'/vendor/autoload.php';

$_SESSION['author'] = getAuthor();
$client  = getS3Client();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	postImage($client);
}

$images = array();
foreach (getObjects($client) as $object) {
	$headers = $client->headObject(['Bucket' => AWS_BUCKET, 'Key' => $object['Key']]);
	$images[] = array(
		'url' => $client->getObjectUrl(AWS_BUCKET, $object['Key']),
		'author' => $headers['Metadata']['author'],
		'date' => date('d/m/Y à H:i:s',strtotime($headers['LastModified'])),
		'type' => explode("_",$object['Key'])[0]
	);
}
include 'view/index.html';
