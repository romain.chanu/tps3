<?php

use Aws\S3\S3Client;
function getAuthor(){
	return  (isPostExist('author')) ? $_POST['author'] : null;
}

function isPostExist($key){
	return (isset($_POST[$key])) ? true : false;
}


function wGetImageSize(){
	return (isset($_FILES["fileToUpload"]["tmp_name"])) ? getimagesize($_FILES["fileToUpload"]["tmp_name"]) : false;
}

function uploadIsImage(){
	return (isPostExist('submit')) ? wGetImageSize() : false;
}

function postImageToS3($client){
	return (uploadIsImage()) ? wPutObject($client) : false;
}

function wPutObject($client){
	return $client->putObject([
		'Bucket' => AWS_BUCKET,
		'Key' => $_POST['radioMeta']."_".basename( $_FILES["fileToUpload"]["name"]),
		'Body'   => fopen($_FILES["fileToUpload"]["tmp_name"], 'r'),
		'Metadata' => array('author' => $_POST['author']),
	]);
}
function postImage($client){
	return (isPostExist('submit')) ? postImageToS3($client) : false;
}

function getS3Client(){
	try{
		// Instantiate the S3 class and point it at the desired host
	    $client = new S3Client([
	        'region' => 'us-west-2',
	        'version' => '2006-03-01',
	        'endpoint' => AWS_ENDPOINT,
	        'credentials' => [
	            'key' => AWS_KEY,
	            'secret' => AWS_SECRET_KEY
	        ],
	        // Set the S3 class to use objects.dreamhost.com/bucket
	        // instead of bucket.objects.dreamhost.com
	        'use_path_style_endpoint' => true
	    ]);
	}catch(Exception $e){
		die("Exception thrown your app/config.php is misconfigured:<br>$e");
	}
	return $client;
}

function getObjects($client){
	return getObjectsListReponse($client)['Contents'] ?? [];
}
function getObjectsListReponse($client){
	return (isPostExist('filter'))
		? $client->listObjects(['Bucket' => AWS_BUCKET,'Prefix' => $_POST['radioFilter']])
		: $objectsListResponse = $client->listObjects(['Bucket' => AWS_BUCKET]);

}
